﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RosterCheck.Classes;
using RosterCheck.Classes.CharacterClasses;

namespace RosterCheck.Windows
{
    /// <summary>
    /// Interaction logic for CheckGearWindow.xaml
    /// </summary>
    public partial class CheckGearWindow : Window
    {
        public CheckGearWindow()
        {
            InitializeComponent();
        }

        private void CheckGearWindow1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var mainWindow1 = new MainWindow();
            mainWindow1.Show();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
//            // Get the guild ranks of all members
            var guildMembers = CharacterJson.GetCharacterGuildRank(tbRealm.Text, tbGuildName.Text);
            // Create a character list which will be displayed in the datagrid
            var characterList = new List<Character>();

            // Loop through each guild member
            foreach (var member in guildMembers)
            {
                // If the guild member has a higher rank than an ?(3), add it to the character list
                if (member.GuildRank >= 3) continue;
                var character = CharacterJson.GetCharacter(member.Name, tbRealm.Text);

                // If the character is not null (below level 100)
                if (character == null) continue;
                character.GuildRank = member.GuildRank;
                characterList.Add(character);
            }

            // Set the source of the datagrid to the character list
            DgCheckGear.ItemsSource = characterList;
        }
    }
}