﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using RosterCheck.Classes;
using System.ComponentModel;
using RosterCheck.Windows;

namespace RosterCheck
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnCheckGear_Click(object sender, RoutedEventArgs e)
        {
            // Invoke the CheckGear window and close the startscreen
            var checkGearWindow1 = new CheckGearWindow();   
            MainWindow1.Close();
            checkGearWindow1.Show();
        }

        private void MainWindow1_KeyUp(object sender, KeyEventArgs e)
        {
            // Checks for escape key press for application shutdown
            if (e.Key == Key.Escape)
            {
                MainWindow1.Close();
            }
        }
    }
}
