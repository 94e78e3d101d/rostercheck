﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RosterCheck.Classes.ItemClasses
{
    public class Items
    {
        public int AverageItemLevel { get; set; }
        public int AverageItemLevelEquipped { get; set; }
        public Back Back { get; set; }
        public Chest Chest { get; set; }
        public Feet Feet { get; set; }
        public Finger1 Finger1 { get; set; }
        public Finger2 Finger2 { get; set; }
        public Hands Hands { get; set; }
        public Legs Legs { get; set; }
        public MainHand MainHand { get; set; }
        public Neck Neck { get; set; }
        public Shoulder Shoulder { get; set; }
        public Trinket1 Trinket1 { get; set; }
        public Trinket2 Trinket2 { get; set; }
        public Waist Waist { get; set; }
        public Wrist Wrist { get; set; }
    }

    public class Back
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }

    public class Chest
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }



    public class Feet
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }


    public class Finger1
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }



    public class Finger2
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }



    public class Hands
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }


    public class Legs
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }


    public class MainHand
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }


    public class Neck
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }

    public class Shoulder
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }


    public class Trinket1
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }


    public class Trinket2
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }


    public class Waist
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }


    public class Wrist
    {
        public string Icon { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public int ItemLevel { get; set; }
    }


    public class Talent
    {
        public Spec Spec { get; set; }
    }

    public class Spec
    {
        public string Name { get; set; }
        public int Order { get; set; }
        public string Role { get; set; }
    }
}
