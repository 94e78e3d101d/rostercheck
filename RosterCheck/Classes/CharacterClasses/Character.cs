﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using Newtonsoft.Json;
using RosterCheck.Classes.ItemClasses;

namespace RosterCheck.Classes.CharacterClasses
{
    public class Character
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public int AverageItemLevelEquipped { get; set; }
        public string PrimaryRole { get; set; }
        public string PrimarySpec { get; set; }
        public string SecondaryRole { get; set; }
        public string SecondarySpec { get; set; }
        public int GuildRank { get; set; }
        public int AverageItemLevel { get; set; }
        public string Realm { get; set; }
        public int Level { get; set; }
}

    public class CharacterJson
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public string Level { get; set; }
        public Items Items { get; set; }
        public List<Talent> Talents { get; set; }

        public enum Realm
        {
            Drakthul,
            Burningblade
        }

        public enum ClassName
        {
            Warrior = 1,
            Paladin = 2,
            Hunter = 3,
            Rogue = 4,
            Priest = 5,
            DeathKnight = 6,
            Shaman = 7,
            Mage = 8,
            Warlock = 9,
            Monk = 10,
            Druid = 11
        }

        /// <summary>
        /// Gets the stream content from armory
        /// </summary>
        /// <param name="requestString">The request string which is queried by the armory API.</param>
        /// <returns>Character json stream content.</returns>
        public static string GetCharacterJsonData(string requestString)
        {
            try
            {
                // Create a request for the URL. Get the character data first.
                var request = WebRequest.Create(requestString);

                // Get the response.
                var response = request.GetResponse();

                // Display the status.
                // string status = ((HttpWebResponse)response).StatusDescription;

                // Get the stream
                var dataStream = response.GetResponseStream();

                // Open the stream using a StreamReader for easy access.

                if (dataStream != null)
                {
                    var reader = new StreamReader(dataStream);

                    // Read the content.
                    var characterStreamContent = reader.ReadToEnd();

                    // Close the stream and the connection.
                    reader.Close();
                    response.Close();

                    return characterStreamContent;
                }
                else
                {
                    return "ERROR - DATASTREAM IS NULL";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                // var errorCharacter = new Character() { Name = "ERROR" };
                return "ERROR - NO STREAM CONTENT";
            }
        }

        /// <summary>
        /// Extracts a character from the character json stream content.
        /// </summary>
        /// <param name="characterName">The characters nickname.</param>
        /// <param name="realm">The characters realm.</param>
        /// <returns>A character object with deserialized values.</returns>
        public static Character GetCharacter(string characterName, string realm)
        {
            // http://eu.battle.net/api/wow/character/maelstrom/Przlator?fields=progression
            // useful fields: progression, items, talents, audit, ?

            // Build the request string with character name and realm parameters.
            string characterRequestString =
                $"http://eu.battle.net/api/wow/character/{realm}/{characterName}?fields=items,talents";

            // Try to fetch the character from armory
            try
            {   
                // Get the character stream content from armory
                var characterStreamContent = GetCharacterJsonData(characterRequestString);

                // Deserialize the json character data.
                var deserializedCharacter = JsonConvert.DeserializeObject<CharacterJson>(characterStreamContent);

                // Get the class ID and convert it to a class name.
                var classId = int.Parse(deserializedCharacter.Class);
                deserializedCharacter.Class = Enum.GetName(typeof (ClassName), classId);

                // Create a new character from the serialized json data, return a valid character only if it is level 100
                if (deserializedCharacter.Level == "100")
                {
                    var importedCharacter = new Character()
                    {
                        Name = deserializedCharacter.Name,
                        Class = deserializedCharacter.Class,
                        Level = int.Parse(deserializedCharacter.Level),
                        Realm = realm,
                        PrimarySpec = deserializedCharacter.Talents[0].Spec.Name,
                        PrimaryRole = deserializedCharacter.Talents[0].Spec.Role,
                        SecondarySpec = deserializedCharacter.Talents[1].Spec.Name,
                        SecondaryRole = deserializedCharacter.Talents[1].Spec.Role,
                        AverageItemLevel = deserializedCharacter.Items.AverageItemLevel,
                        AverageItemLevelEquipped = deserializedCharacter.Items.AverageItemLevelEquipped,
                    };
                    return importedCharacter;
                }

                // Else return a null character
                else return null;                
            }

            // If any errors occur, add an ERROR character to the list
            catch (Exception ex)
            {
                MessageBox.Show(characterName + " " + ex);
                var errorCharacter = new Character() { Name = "ERROR" };
                return errorCharacter;
            }
        }

        /// <summary>
        /// Extracts a list of guild characters.
        /// </summary>
        /// <param name="realm">Realm name on which the guild is on.</param>
        /// <param name="guildName">Guild name of the guild.</param>
        /// <returns>A list of guild characters with specified rank.</returns>
        public static List<Character> GetCharacterGuildRank(string realm, string guildName)
        {
            var guildRankRequestString =
                $"http://eu.battle.net/api/wow/guild/{realm}/{guildName}?fields=members";

            // Initialize the guild members list
            var guildMembers = new List<Character>();

            // Try to fetch the character from armory
            try
            {
                // Get the character stream content from armory
                var characterStreamContent = GetCharacterJsonData(guildRankRequestString);

                // Deserialize the json character data.
                var deserializedGuild = JsonConvert.DeserializeObject<Guild>(characterStreamContent);

                // Loop through all members and add them to the list
                guildMembers = deserializedGuild.Members.Select(member => new Character()
                {
                    Name = member.Character.Name, GuildRank = member.Rank
                }).ToList();

                return guildMembers;
            }
            // If any errors occur, add an ERROR character to the list
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                var errorCharacter = new Character() {Name = "ERROR"};
                guildMembers.Add(errorCharacter);
                return guildMembers;
            }
        }
    }

} 
    
