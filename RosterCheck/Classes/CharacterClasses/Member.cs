﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RosterCheck.Classes.CharacterClasses
{
    internal class Member
    {
        public Character Character { get; set; }
        public int Rank { get; set; }
    }
}
