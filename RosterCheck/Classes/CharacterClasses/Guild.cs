﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RosterCheck.Classes.CharacterClasses
{
    class Guild
    {
        public List<Member> Members { get; set; }
        public string Name { get; set; }
        public string Realm { get; set; }
        public int Side { get; set; }
    }
}
